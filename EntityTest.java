import org.jsfml.graphics.Color;
import org.jsfml.graphics.RenderWindow;
import org.jsfml.system.Vector2f;
import org.jsfml.window.Keyboard;
import org.jsfml.window.VideoMode;
import org.jsfml.window.event.Event;
import org.jsfml.window.event.KeyEvent;
import org.jsfml.window.event.Event.Type;

public class EntityTest {
    public static void main(String[] args) {
        RenderWindow window = new RenderWindow(new VideoMode(640 /* x */, 480 /* y */), "Test");
        World w = new World(10f, 0.95f);
        Entity d = new Entity(15f, 40f, new Vector2f(100, 100));
        Entity d1 = new Entity(10f, 30f, new Vector2f(180, 100));
        w.addEntity(d);
        w.addEntity(d1);
        while (window.isOpen()) {
            for (Event e : window.pollEvents()) {
                if (e.type == Type.KEY_PRESSED) {
                    KeyEvent me = e.asKeyEvent();
                    if (Keyboard.isKeyPressed(Keyboard.Key.ESCAPE)) {
                        window.close();
                    }

                    if (me.key == Keyboard.Key.SPACE) {
                        if (d.isGrounded()) {
                            d.setVelocity(new Vector2f(d.getVelocity().x, d.getVelocity().y - d.getJumpSpeed()));
                            d.isGrounded(false);
                        }
                        if (d1.isGrounded()) {
                            d1.setVelocity(new Vector2f(d1.getVelocity().x, d1.getVelocity().y - d1.getJumpSpeed()));
                            d1.isGrounded(false);
                        }
                    } else if (me.key == Keyboard.Key.S) {
                    }

                    if (me.key == Keyboard.Key.A) {
                        d.setVelocity(new Vector2f(-d.getMoveSpeed(), d.getVelocity().y));

                        d1.setVelocity(new Vector2f(-d1.getMoveSpeed(), d1.getVelocity().y));
                    } else if (me.key == Keyboard.Key.D) {
                        d.setVelocity(new Vector2f(d.getMoveSpeed(), d.getVelocity().y));

                        d1.setVelocity(new Vector2f(d1.getMoveSpeed(), d1.getVelocity().y));
                    }
                }
            }
            window.clear(Color.BLUE);
            w.updateWorld(window);
            window.display();
        }
    }
}
