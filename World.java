import java.util.ArrayList;
import org.jsfml.graphics.RenderWindow;
import org.jsfml.system.Vector2f;

public class World {
    private final int DELAY = 20;
    private ArrayList<Entity> entities = new ArrayList<>();
    public boolean update;
    public final float gravity;
    public final float friction;

    public World(float g, float f) {
        gravity = g;
        friction = f;
        update = true;
    }

    public void updateWorld(RenderWindow w){
        for (Entity e : entities) {
            e.move();
            applyPhysics(e);
            w.draw(e.getDrawable());
            pause();
        }
        
    }
    public void pauseWorld() {
        update = false;
    }

    public void resumeWorld() {
        update = true;
    }

    public void addEntity(Entity e) {
        entities.add(e);
    }

    public void removeEntity(Entity e) {
        entities.remove(e);
    }

    private void applyPhysics(Entity e) {
        if(!update){
            return;
        }
        float ground = 400f;// screen.getGround(e);
        if (e.getPosition().y < ground) {
            e.setPosition(e.getPosition().x, e.getPosition().y + e.getVelocity().y);
            e.setVelocity(new Vector2f(e.getVelocity().x, e.getVelocity().y + gravity));
        } else {
            e.setPosition(e.getPosition().x, ground);
            e.isGrounded(true);
            if (Math.abs(e.getVelocity().x) < 0.5) {
                e.setVelocity(new Vector2f(0, 0));
            } else {
                e.setVelocity(new Vector2f(e.getVelocity().x * friction, 0));
            }
        }
    }

    private void pause(){
        try{
            Thread.sleep(DELAY);
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
