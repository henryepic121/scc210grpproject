import org.jsfml.graphics.*;
import org.jsfml.system.*;
import java.nio.file.*;

class MenuElement extends Sprite{
	private Texture t;
	private int lvl;

	public MenuElement(Vector2f vPos, Vector2i vSize,String path,int lvl){
		t = new Texture();
		this.lvl = lvl;
		this.setPosition(vPos);

		try{
			t.loadFromFile(Paths.get(path));
		}
		catch(Exception e){
			e.printStackTrace();
		}		

		this.setTexture(t);
		this.setTextureRect(new IntRect(new Vector2i(0,0),vSize));
	}

	public void wasClicked(Screen s){
		s.setLevel(lvl);
	}
}

public class Menu {
	private MenuElement[] titleScreen = new MenuElement[5];
	private MenuElement[] inGameMenu = new MenuElement[4];

	public Menu(){
		Texture bg1 = new Texture();
		try{
		bg1.loadFromFile(Paths.get("scc210grpproject/assets/TEST BG.png"));
		} catch(Exception e){
			e.printStackTrace();
		}
		titleScreen[0] = new MenuElement(new Vector2f(0,0), new Vector2i(1600, 900),"scc210grpproject/assets/TEST BG.png",0); 
		titleScreen[1] = new MenuElement(new Vector2f(600,500), new Vector2i(400, 50),"scc210grpproject/assets/TEST Menu.png",0); 
		titleScreen[2] = new MenuElement(new Vector2f(600, 600), new Vector2i(400, 50),"scc210grpproject/assets/TEST Menu.png",0); 
		titleScreen[3] = new MenuElement(new Vector2f(600, 700), new Vector2i(400, 50),"scc210grpproject/assets/TEST Menu.png",0); 
		titleScreen[4] = new MenuElement(new Vector2f(600, 800), new Vector2i(400, 50),"scc210grpproject/assets/TEST Menu.png",0);
		
		inGameMenu[0] = new MenuElement(new Vector2f(0,0), new Vector2i(1600, 900),"scc210grpproject/assets/TEST BG.png",0); 
		inGameMenu[1] = new MenuElement(new Vector2f(600,500), new Vector2i(400, 50),"scc210grpproject/assets/TEST Menu.png",-1); 
		inGameMenu[2] = new MenuElement(new Vector2f(600, 600), new Vector2i(400, 50),"scc210grpproject/assets/TEST Menu.png",-1); 
		inGameMenu[3] = new MenuElement(new Vector2f(600, 700), new Vector2i(400, 50),"scc210grpproject/assets/TEST Menu.png",-1); 
	}
	
	public MenuElement[] getMenu(int id){
		MenuElement[] me = null;
		switch (id){
			case -1:
				 me = titleScreen;
				break;
			case 0:
				me = inGameMenu;
			   break;
			default:
				me = titleScreen;
		}
		return me;
	}
}
