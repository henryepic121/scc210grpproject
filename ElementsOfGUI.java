import java.util.*;
import org.jsfml.graphics.*;

public class ElementsOfGUI {
	private Screen scr;
	private Menu menus = new Menu();
	private LevelParser levelParser = new LevelParser();

	public ElementsOfGUI(Screen s) {
		scr = s;
	}

	public ArrayList<Drawable> getListToDraw() {
		ArrayList<Drawable> tempList = new ArrayList<Drawable>();
		int lvl = scr.getLevel();

		if (lvl > 0) {		// menus smaller then 0
			int[][] lvlMatrix = levelParser.parseLevel(lvl);
			//code for lvl
		} else {
			MenuElement[] menu = menus.getMenu(lvl);
			if (menu != null) {
				for (MenuElement m : menu) {
					tempList.add(m);
				}
			}
		}
		return tempList;
	}
}
