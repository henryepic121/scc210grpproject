import org.jsfml.graphics.Color;
import org.jsfml.graphics.Drawable;
import org.jsfml.graphics.RectangleShape;
import org.jsfml.system.Vector2f;

public class Entity {
    private RectangleShape rect;        //for testing
    private float moveSpeed , jumpSpeed ;
    private Vector2f velocity = new Vector2f(0, 0);
    private boolean grounded = false;

    public Entity(float m, float j, Vector2f pos) {
        moveSpeed = m;
        jumpSpeed = j;
        rect = new RectangleShape(new Vector2f(30, 30));
        rect.setPosition(pos);
        rect.setFillColor(Color.RED);
    }

    public void move(){
        rect.move(velocity);
    }

    public Drawable getDrawable(){
        return rect;
    }

	public Vector2f getPosition() {
		return rect.getPosition();
    }
    
    public void setPosition(float x, float y) {
		rect.setPosition(x,y);
	}

	public void setVelocity(Vector2f v) {
        velocity = v;
	}

	public Vector2f getVelocity() {
		return velocity;
	}

	public void isGrounded(boolean b) {
        grounded = b;
    }
    
    public boolean isGrounded() {
        return grounded;
	}

	public float getMoveSpeed() {
		return moveSpeed;
	}

	public float getJumpSpeed() {
		return jumpSpeed;
	}
}
