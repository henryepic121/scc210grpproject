import org.jsfml.graphics.*;
import org.jsfml.window.*;
import org.jsfml.window.event.*;
import org.jsfml.window.event.Event.Type;
import org.jsfml.system.*;
import java.util.*;

public class Screen {
	private RenderWindow window = new RenderWindow();
	private ArrayList<Drawable> elements = new ArrayList<Drawable>();	// returned from ElementsOfGUI, has every element to be drawn
	private ElementsOfGUI elementsOfGUI = new ElementsOfGUI(this);
	private ArrayList<Drawable> sprites = new ArrayList<Drawable>(); //for projectiles , characters, enemies ...
	private int level = -1;

	public Screen(){
		window.create(new VideoMode(1600,900,8),"Best Game Ever");
		try{
		window.setActive();
		} catch(Exception e){
			e.printStackTrace();
		}
		elements = elementsOfGUI.getListToDraw();
	}

	public void playGame(){
		while(window.isOpen()){
			//main loop

			Iterable<Event> eList = window.pollEvents();
			for(Event e : eList){
				MouseButtonEvent me = e.asMouseButtonEvent();
				if(me != null){
					if(e.type == Event.Type.MOUSE_BUTTON_PRESSED){
						if(me.button == Mouse.Button.LEFT)
						doAction(Mouse.getPosition(window));
						break;
					}
				}
				KeyEvent ke = e.asKeyEvent();
				if(ke != null){
					if(ke.key == Keyboard.Key.ESCAPE){
						window.close();	
					}
				}
				if(e.type == Type.CLOSED){
					window.close();	
				}
			}

			elements = elementsOfGUI.getListToDraw();
			draw();
			window.display(); //refresh everything
			pause();
		}
	}

	public int getLevel(){
		return level;
	}

	public ArrayList<Drawable> getSprites(){
		return sprites;
	}

	public void draw(){
		if(elements == null){
			System.out.println("Elements are null");
			return;
		}
		for(Drawable d : elements){
			window.draw(d);
		}
	}

	public void setLevel(int l){
		this.level = l;
	}

	private void doAction(Vector2i pos){
		for(int i = 1; i < elements.size();i++){// start at 1 since 0 is background
			Drawable d = elements.get(i);
			if(d instanceof MenuElement){
				MenuElement me = (MenuElement) d;
				if(containsPos(me, pos)){
					me.wasClicked(this);
					return;
				}
			}
		}
		return;
	}

	private void pause(){
		try{
			Thread.sleep(50);
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	private boolean containsPos(Sprite s, Vector2i pos){
		if(pos.x > s.getPosition().x && pos.x < s.getPosition().x + s.getTexture().getSize().x){
			if(pos.y > s.getPosition().y && pos.y < s.getPosition().y + s.getTexture().getSize().y){
				return true;
			}
		}
		return false;
	}
}

